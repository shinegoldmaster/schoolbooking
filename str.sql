/*
SQLyog Enterprise - MySQL GUI v8.12 
MySQL - 5.5.5-10.1.10-MariaDB : Database - almaqraa_booking
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`almaqraa_booking` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `almaqraa_booking`;

/*Table structure for table `librarycategory` */

DROP TABLE IF EXISTS `librarycategory`;

CREATE TABLE `librarycategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` char(254) DEFAULT NULL,
  `types` smallint(2) DEFAULT '0' COMMENT '0-gita  1-quran',
  `menu_id` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `libraryitems` */

DROP TABLE IF EXISTS `libraryitems`;

CREATE TABLE `libraryitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(254) DEFAULT NULL,
  `sub_cat_id` int(11) DEFAULT NULL,
  `mp3_link` varchar(254) DEFAULT '0',
  `pdf_link` varchar(254) DEFAULT '0',
  `ms_link` varchar(254) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `librarysubcategory` */

DROP TABLE IF EXISTS `librarysubcategory`;

CREATE TABLE `librarysubcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_cat_name` varchar(254) DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `quranmenu` */

DROP TABLE IF EXISTS `quranmenu`;

CREATE TABLE `quranmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` char(254) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
