<?php

/***************************************************************/
/***************** Start Frontend Router ***********************/
/***************************************************************/
Route::get('/', 'MainController@index');
Route::get('/main', 'MainController@index');
Route::get('/about','MainController@getAboutUsPageData');
Route::get('/program', 'MainController@getInitialProgramList');
Route::get('/program/instructor/{id}', 'MainController@instructorsShow');
Route::get('/program/appointments/{id}', 'MainController@appointments');
Route::get('/news', 'MainController@getNewsData');
Route::get('/news/details/{id}', 'MainController@newsDetail');
Route::get('/help', 'MainController@helpPage');
Route::get('/stats', 'MainController@getStatsPageData');
Route::get('/voicechat',  function(){return view('layouts/voicechat');});

Route::get('/librarys', function(){return view('library/library');});
Route::get('/librarys/get-item/{id}', 'MainController@getLibraryItem');
Route::get('/librarys/play-audio/{id}', 'MainController@playLibraryAudio');

Route::get('/quran', 'MainController@quranPageManagement');
Route::get('/quran/{id}', 'MainController@quranPageManagement');
Route::get('/quran/get-subcategory/{id}', 'MainController@getQuranSubcategory');
Route::get('/quran/get-item/{id}', 'MainController@getQuranItem');
Route::get('/quran/play-audio/{id}', 'MainController@playQuranAudio');
/***************************************************************/
/************** Start Admin Dashboard Router *******************/
/***************************************************************/
Route::get('/admin/dashboard', 'AdminController@index');
/////////////////////////// user management////////////////
Route::get('/admin/usermanagement', 'AdminController@userManagement');
Route::get('/admin/usermanagement/show-user/{id}', 'AdminController@showUserList');
Route::post('/admin/user-delete', 'AdminController@userDelete');
Route::post('/admin/user-update', 'AdminController@userUpdate');

///////////////////////// category management////////////////
Route::get('/admin/categorymanagement', 'AdminController@categoryManagement');
Route::post('/admin/category-delete', 'AdminController@categoryDelete');
Route::post('/admin/category-edit-save', 'AdminController@categoryEditSave');
Route::post('/admin/category-new-save', 'AdminController@categoryNewSave');
Route::get('/admin/categorymanagement/category-edit/{id}', 'AdminController@categoryEdit');
Route::get('/admin/categorymanagement/category-new', 'AdminController@categoryNew');

/////////////////////// subcategory management////////////////
Route::get('/admin/subcategorymanagement', 'AdminController@subcategoryManagement');
Route::post('/admin/subcategory-delete', 'AdminController@subcategoryDelete');
Route::post('/admin/subcategory-edit-save', 'AdminController@subcategoryEditSave');
Route::post('/admin/subcategory-new-save', 'AdminController@subcategoryNewSave');
Route::get('/admin/subcategorymanagement/subcategory-edit/{id}', 'AdminController@subcategoryEdit');
Route::get('/admin/subcategorymanagement/subcategory-new', 'AdminController@subcategoryNew');

//////// assign subcategory VS instructor management/////////
Route::get('/admin/assignsubcategory', 'AdminController@assignManagement');
Route::post('/admin/update-subcategorytime', 'AdminController@updateSubcategoryTime');
Route::post('/admin/assign-subcategory-delete', 'AdminController@deleteAssignSubcategory');
Route::post('/admin/assign-subcategory-insturctor', 'AdminController@assignSubcategoryAndInstructory');

/////////////////////////// appointment management////////////////
Route::get('/admin/appointmentmanagement', 'AdminController@appointmentsManagement');
Route::post('/admin/appointment-delete', 'AdminController@deleteappointment');
Route::post('/admin/appointment-register', 'AdminController@registerappointment');

/////////////////////////// news management////////////////
Route::get('/admin/newsmanagement', 'AdminController@newsManagement');
Route::post('/admin/news-delete', 'AdminController@newsDelete');
Route::post('/admin/news-edit-save', 'AdminController@newsEditSave');
Route::post('/admin/news-new-save', 'AdminController@newsNewSave');
Route::get('/admin/newsmanagement/news-edit/{id}', 'AdminController@newsEdit');
Route::get('/admin/newsmanagement/news-new', 'AdminController@newsNew');

/////////////////////////// Message management////////////////
Route::get('/admin/messagemanagement', 'AdminController@messageManagement');
Route::post('/admin/messagemanagement', 'AdminController@messageManagement');

/////////////////////// library management////////////////
Route::get('/admin/librarymanagement', 'AdminController@libraryManagement');
Route::get('/admin/quranmenu', 'AdminController@quranMenuManagement');
Route::post('/admin/library/quranmenu-delete', 'AdminController@quranMenuDelete');
Route::post('/admin/library/quranmenu-edit-save', 'AdminController@quranMenuEditSave');
Route::post('/admin/library/quranmenu-new-save', 'AdminController@quranMenuNewSave');

Route::get('/admin/librarycategory', 'AdminController@libraryCategoryManagement');
Route::post('/admin/library/librarycategory-delete', 'AdminController@libraryCategoryDelete');
Route::post('/admin/library/librarycategory-edit-save', 'AdminController@libraryCategoryEditSave');
Route::post('/admin/library/librarycategory-new-save', 'AdminController@libraryCategoryNewSave');

Route::get('/admin/librarysubcategory', 'AdminController@librarySubCategoryManagement');
Route::post('/admin/library/librarysubcategory-delete', 'AdminController@librarySubCategoryDelete');
Route::post('/admin/library/librarysubcategory-edit-save', 'AdminController@librarySubCategoryEditSave');
Route::post('/admin/library/librarysubcategory-new-save', 'AdminController@librarySubCategoryNewSave');


Route::get('/admin/libraryitems', 'AdminController@libraryItemsManagement');
Route::post('/admin/libraryitems-delete', 'AdminController@libraryItemsDelete');
Route::post('/admin/libraryitems-edit-save', 'AdminController@libraryItemsEditSave');
Route::post('/admin/libraryitems-new-save', 'AdminController@libraryItemsNewSave');
Route::get('/admin/libraryitems/libraryitems-edit/{id}', 'AdminController@libraryItemsEdit');
Route::get('/admin/libraryitems/libraryitems-new', 'AdminController@libraryItemsNew');


/***************************************************************/
/************ Start Student Dashboard Router *******************/
/***************************************************************/
Route::get('/student', 'StudentController@index');
Route::get('/student/student-info-show', 'StudentController@studentInfoShow');
Route::post('student/info-update', 'StudentController@studentInfoUpdate');

/////////////////////////// booking and voice room////////////////
Route::get('/student/programs-show', 'StudentController@index');
Route::get('/student/instructors-show', 'StudentController@instructorsShow');
Route::get('/student/appointments/{id}', 'StudentController@appointments');
Route::get('/student/show-homework-audio/{id}', 'StudentController@showHomeworkAudio');
Route::post('/student/join', 'StudentController@appointmentBooking');
Route::post('/student/appointment-cancel', 'StudentController@appointmentCancel');
Route::get('/student/appointments-history', 'StudentController@appointmentHistory');
Route::get('/student/voice-room', 'StudentController@voiceRoom');

/////////////////////////// homework management////////////////
Route::get('/student/homework-add', 'StudentController@getAvailableAppointmentDataForAddHomework');
Route::post('/student/homeworks-add','StudentController@addHomework');
Route::get('/student/homework-history',  'StudentController@showHomeworkHistory');
Route::post('/student/homework-history',  'StudentController@showHomeworkHistory');

/////////////////////////// msg management////////////////
Route::get('/student/msg-send','StudentController@getRecipienterList');
Route::post('/student/sendmessage','StudentController@sendMessages');
Route::get('/student/msgs-history', 'StudentController@showSendMessageHistory');
Route::post('/student/msgs-history', 'StudentController@showSendMessageHistory');
Route::get('/student/msgs-received', 'StudentController@showReceivedMessageHistory');
Route::post('/student/msgs-received', 'StudentController@showReceivedMessageHistory');

/***************************************************************/
/********* Start Instructor Dashboard Router *******************/
/***************************************************************/
Route::get('/instructor', 'InstructorController@index');
Route::get('/instructor/instructor-info-show', 'InstructorController@instructorInfoShow');
Route::post('instructor/info-update', 'InstructorController@instructorInfoUpdate');

////////////////////// joinbooking management////////////////
Route::post('/instructor/join', 'InstructorController@bookedJoin');
Route::get('/instructor/voice-room', 'InstructorController@voiceRoom');
Route::post('/instructor/voice-room-end', 'InstructorController@voiceRoomEnd');
Route::get('/instructor/programs-show', 'InstructorController@index');
Route::get('/instructor/joined-history', 'InstructorController@joinedHistory');
Route::post('/instructor/joined-cancel', 'InstructorController@bookedjoinCancel');
Route::get('/instructor/show-homework-audio/{id}', 'InstructorController@showHomeworkAudio');

////////////////////// homework management////////////////
Route::get('/instructor/homework-add', 'InstructorController@getAvailableAppointmentData');
Route::post('/instructor/homeworks-add','InstructorController@addHomework');
Route::get('/instructor/homework-history',  'InstructorController@showHomeworkHistory');
Route::post('/instructor/homework-history',  'InstructorController@showHomeworkHistory');

////////////////////// message management////////////////
Route::get('/instructor/msg-send','InstructorController@getRecipienterList');
Route::post('/instructor/sendmessage','InstructorController@sendMessages');
Route::get('/instructor/msgs-history', 'InstructorController@showMessageHistory');
Route::post('/instructor/msgs-history', 'InstructorController@showMessageHistory');
Route::get('/instructor/msgs-received', 'InstructorController@showReceivedMessageHistory');
Route::post('/instructor/msgs-received', 'InstructorController@showReceivedMessageHistory');

////////////////////// follow up management////////////////
Route::get('/instructor/show-student-list','InstructorController@getOwnStudentList');
Route::get('/instructor/followup/{id}','InstructorController@getFollowUpData');
Route::post('/instructor/followupdate','InstructorController@updateFollowUpDate');

/***************************************************************/
/************** Language Processing Router *********************/
/***************************************************************/

Route::post('/language/{locale}', array(
	'MiddlewareGroup' => 'LanguageSwitcher',
	'uses' => 'LanguageController@index'
));
/***************************************************************/
/*********************** Start Cron  Router *********************/
/***************************************************************/
Route::get('/cron/time-status-manage','CronController@timeManageByCron');

/***************************************************************/
/*********************** Start Chat  Router *********************/
/***************************************************************/
Route::post('/chat/{option}/{id}', 'ChatController@index');

/***************************************************************/
/******************* Start Ajax and js Router ********************/
/***************************************************************/
Route::post('/getcountry', 'MainController@getCountryData');
Route::post('/getcountryforupdatestudent', 'MainController@getCountryDataForUpdateStudent');
Route::post('/getcategorylist', 'MainController@getCategoryList');
Route::post('/searchSubcategoryData/{flag}/{id}', 'MainController@getSubcategoryListBySearch');


/***************************************************************/
/*********************** Start Auth Router *********************/
/***************************************************************/
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
