<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$account_type = Auth::user()->status;		
		if($account_type == 3){
			return redirect()->action('AdminController@index');			
		}else if($account_type == 2){
			return redirect()->action('StudentController@index');			
		}else if($account_type == 1){
			return redirect()->action('InstructorController@index');			
		}else{
			return redirect()->action('StudentController@index');			
		}        
    }
	
}
